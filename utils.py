r"""**Utilities for CNN Basics Microcredit**"""

import os
import numpy as np
from PIL import Image


def get_image(generated: bool = True, scale: int = 1, channels: int = 1):
    r"""**Retrieve our sample image, or an arbitrary generated image**
    :param generated: whether to generate an arbitrary image or not
    :param size: size of the generated image in %
    :param channels: number of channels of the generated image
    """
    _path = 'images/basic_forms.png'  # hardcoded default image path
    _default_size = 100  # 100x100 pixels
    _channels = channels
    _scale = min(max(scale, 0.25), 2)
    # Check if sample image exists
    if not os.path.exists(_path) or generated:
        # Generate an arbitrary image from random data
        image = np.random.randint(0, 256, size=(_default_size, _default_size), dtype=np.uint8) if channels == 1 else np.random.randint(0, 256, size=(_default_size, _default_size, 3), dtype=np.uint8)
    else:
        # Load the sample image
        image = Image.open(_path) if _channels == 3 else Image.open(_path).convert("L")
        _h, _w = int(image.size[0] * _scale), int(image.size[1] * _scale)
        image = image.resize((_h, _w))
        image = np.array(image)

    return image



def convolve(image, kernel, padding: bool = False):
    r"""**Perform convolution**
    :param image: input image
    :param kernel: kernel to use
    :param padding: whether to use padding or not
    """
    h, w = image.shape
    kh, kw = kernel.shape
    
    # Apply padding
    #if padding:
    #    ph = (kh - 1) // 2  # Padding height
    #    pw = (kw - 1) // 2  # Padding width
    
    #    image = np.pad(image, ((ph, ph), (pw, pw)), mode='constant')
    
    output = np.zeros_like(image)
    
    for i in range(h - kh + 1):
        for j in range(w - kw + 1):
            output[i, j] = np.sum(image[i:i+kh, j:j+kw] * kernel)
      
    return output
